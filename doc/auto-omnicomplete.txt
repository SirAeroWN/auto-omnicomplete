*auto-omnicomplete.txt*

CONTENTS~
                                            *auto-omnicomplete-contents*
----------------------------------------------------------------------------
1.  Overview                                 |auto-omnicomplete-overview|
2.  Installation                             |auto-omnicomplete-installation|
3.  Commands                                 |auto-omnicomplete-commands|
4.  Settings                                 |auto-omnicomplete-settings|


OVERVIEW~
                                            *auto-omnicomplete-overview*
----------------------------------------------------------------------------
Auto-omnicomplete leverages Vim's builtin omni-complete function to provide
completion as you type. It is intended to be very light weight.


INSTALLATION~
                                            *auto-omnicomplete-installation*
----------------------------------------------------------------------------
Use the plugin manager of your choice to add this to your runtime path. If
you use vim's builtin packadd command, simply clone into your plugin
directory.

It is strongly recommended that you have `completeopt` set in your vimrc as
follows for the plugin to work properly:
>
    set completeopt=menu,menuone,noinsert,noselect
<
`noselect` and `noinsert` are going to be helpful in particular. `noinsert` prevents vim from
inserting the first match for you. `noselect` prevents vim from automatically
selecting the first match.


SETTINGS~
                                            *auto-omnicomplete-settings*
----------------------------------------------------------------------------
                                            *g:AutoOmniComplete_tag*
                                            *g:AutoOmniComplete_keyword*
                                            *g:AutoOmniComplete_file*
                                            *g:AutoOmniComplete_omni*

Important note:~
All bindings options available here are only applicable when the popup menu is
open. For example, here's how the keword completions are bound:
>
    function! AutoOmniComplete#BindCompleteKeyword(bind)
        let g:AutoOmniComplete_keyword = a:bind
        execute 'inoremap <expr> ' . g:AutoOmniComplete_keyword . ' AutoOmniComplete#CompleteKeyword()'
    endfunction

    function! AutoOmniComplete#CompleteKeyword()
        if pumvisible()
            return "\<c-x>\<c-n>\<c-n>"
        else
            if exists('g:complete_keword')
                return g:complete_keword
            endif
        endif
    endfunction
<

If you want to rebind a completion default completion when the popup menu is not
visible, do so in your vimrc as you would any other binding.

Toggling to other completion modes:~
To rebind keyword, tag and file completion to easier to access keys set the
appropriate variable in your vimrc, making sure to backslash escape the initial
opening bracket. For example to rebind tag completion to `<c-y>`:

>
    let g:AutoOmniComplete_keyword="\<c-y>"
<

By default, these are bound as follows:

>
    if !exists('g:AutoOmniComplete_tag')
        call AutoOmniComplete#BindCompleteTag("\<c-t>")
    else
        call AutoOmniComplete#BindCompleteTag(g:AutoOmniComplete_tag)
    endif
    if !exists('g:AutoOmniComplete_keyword')
        call AutoOmniComplete#BindCompleteKeyword("\<c-k>")
    else
        call AutoOmniComplete#BindCompleteKeyword(g:AutoOmniComplete_keyword)
    endif
    if !exists('g:AutoOmniComplete_file')
        call AutoOmniComplete#BindCompleteFile("\<c-f>")
    else
        call AutoOmniComplete#BindCompleteFile(g:AutoOmniComplete_file)
    endif
    if !exists('g:AutoOmniComplete_omni')
        call AutoOmniComplete#BindCompleteOmni("\<c-o>")
    else
        call AutoOmniComplete#BindCompleteOmni(g:AutoOmniComplete_omni)
    endif
<
It is also possible to write your own expression mapping to do the same binding,
however it is always nice to have a shortcut.

----------------------------------------------------------------------------
                                            *g:AutoOmniComplete_complete_map*

This setting allows you to change the default completion type. Like with the
other settings here, it accepts a double-quoted, backslash escaped sequence of
key bindings. See `:help ins-completion` for bindings. By default this variable
is unset, and the plugin shows Omni-Completion. If set, however, any of the
available completion types could be used. For example, to use keyword completion
instead:
>
    let g:AutoOmniComplete_complete_map"\<c-x>\<c-n>"
<
